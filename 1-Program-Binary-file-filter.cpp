#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

const uint32_t BIG_ENDIAN_ID = 0x4d6d6d4d;
const uint32_t LITTLE_ENDIAN_ID = 0x49696949;
const uint32_t FRAME_ID = 0x46727246;
const uint32_t VIDEO_DATA_ID = 0x56696956;
const uint32_t AUDIO_LIST_ID = 0x416c6c41;
const uint32_t AUDIO_DATA_ID = 0x41757541;
#endif /* __PROGTEST__ */

/*!
 * @brief Function closes files and cleans output file after
 * unsuccessful result of working of program
 * @param fin Output stream
 * @param fout Output stream
 * @param dst String - name of destination file(to clean
 * output file if information was written)
*/
void close_files(fstream& fin, fstream& fout, const char* dst) {
    fin.close();
    fout.close();
    fout.open(dst, ios::out);
    fout.close();
}
/*!
 * @brief Function converts bytes' order 
 * (helps to convert little-endian to big-endian and vice versa)
 * @param num Data represented in 4-byte-number format
*/
void convert_endians(uint32_t& num) {
    num = ((num & 0xFF) << 24) | (((num >> 8) & 0xFF) << 16) |
        (((num >> 16) & 0xFF) << 8) | ((num >> 24) & 0xFF);
}

/*!
 * @brief Function to read two data items(represented in 4 bytes) from input stream
 * @param fin Input stream
 * @param fout Output stream
 * @param buf1 Variable to read first item
 * @param buf2 Variable to read second item
 * @param dst String - output file name (to close and clean file
 * if reading is unsuccessful)
 * @return True if reading was successful, else false
*/

bool read_to(fstream& fin, fstream& fout, uint32_t& buf1,
    uint32_t& buf2, const char* dst) {
    fin.read(reinterpret_cast<char*>(&buf1), sizeof(buf1));
    if (!fin.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    fin.read(reinterpret_cast<char*>(&buf2), sizeof(buf2));
    if (!fin.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}
/*!
 * @brief Function to read one data item(represented in 2 bytes) from input stream
 * @param fin Input stream
 * @param fout Output stream
 * @param buf Variable to read item
 * @param dst String - destination file name (to close and clean file
 * if reading is unsuccessful)
 * @return True if reading was successful, else false
*/
bool read_one(fstream& fin, fstream& fout, char (&buf)[2], const char* dst) {
    fin.read(reinterpret_cast<char*>(&buf), sizeof buf);
    if (!fin.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}
/*!
 * @brief Function to read one data item(represented in 4 bytes) from input stream
 * @param fin Input stream
 * @param fout Output stream
 * @param buf Variable to read item
 * @param dst String - destination file name (for function close_files(...))
 * @return True if reading was successful, else false
*/
bool read_one(fstream& fin, fstream& fout, uint32_t& buf, const char* dst) {
    fin.read(reinterpret_cast<char*>(&buf), sizeof buf);
    if (!fin.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}

/*!
 * @brief Function to open files and make input and output streams
 * @param fin Place for input stream
 * @param fout Place for output stream
 * @param src Source file name
 * @param dst String - destination file name
 * @return True if openning was successful, else false
*/
bool open_files(fstream& fin, fstream& fout,
    const char*& src, const char*& dst) {
    fin.open(src, ios::in | ios::binary);
    fout.open(dst, ios::out | ios::binary);
    if (!fin.good() || !fout.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}

/*!
 * @brief Class represents audio data with methadata about content inside
*/
class AudioData {
    void convAData() {
        convert_endians(this->payload);
    }
public:
    uint32_t id;
    char lang[2];
    uint32_t payload;
     /*!
     * @brief Method sets methadata, checks coding type and converts
     * methadata if it is big-endian
     * @param endian Information about coding type (little- or big- endian)
     * @param fin Input stream
     * @param fout Output stream
     * @param dst String - destination file name (for function close_files(...))
     * @return True if methadata (id of audio data) are right
    */
    bool setAudioData(uint32_t endian, fstream& fin, fstream& fout, const char*& dst) {
        if (this->id != AUDIO_DATA_ID) {
            close_files(fin, fout, dst);
            return false;
        }
        if (endian == BIG_ENDIAN_ID) {
            convAData();
        }
        return true;
    }
};
/*!
 * @brief Class represents audio list with methadata about content inside
*/
class AudioList {
    void convAList(void) {
        convert_endians(this->len);
        convert_endians(this->payload);
    }
public:
    uint32_t id;
    uint32_t len;
    uint32_t payload;
    vector<AudioData> data_in;
    uint32_t old_payload;
    uint32_t old_len;
    /*!
     * @brief Method sets methadata, checks coding type and converts
     * methadata if it is big-endian
     * @param endian Information about coding type (little- or big- endian)
     * @param fin Input stream
     * @param fout Output stream
     * @param dst String - destination file name (for function close_files(...))
     * @return True if methadata (id of audio list) are right
    */
    bool setAudioList(uint32_t endian, fstream& fin, fstream& fout, const char*& dst) {
        if (this->id != AUDIO_LIST_ID) {
            close_files(fin, fout, dst);
            return false;
        }
        if (endian == BIG_ENDIAN_ID) {
            convAList();
        }
        this->old_payload = this->payload;
        this->old_len = this->len;
        this->data_in.resize(this->len);
        return true;
    }
};

/*!
 * @brief Class represents video data with methadata about content inside
*/
class VideoData {
    void videoConv() {
        convert_endians(this->payload);
    }
public:
    uint32_t id;
    uint32_t payload;
    /*!
     * @brief Method sets methadata, checks coding type and converts
     * methadata if it is big-endian
     * @param endian Information about coding type (little- or big- endian)
     * @param fin Input stream
     * @param fout Output stream
     * @param dst String - destination file name (for function close_files(...))
     * @return True if methadata (id of video) are right
    */
    bool setVideoData(uint32_t endian, fstream& fin, fstream& fout, const char*& dst) {
        if (this->id != VIDEO_DATA_ID) {
            close_files(fin, fout, dst);
            return false;
        }
        if (endian == BIG_ENDIAN_ID) {
            videoConv();
        }
        return true;
    }
};
/*!
 * @brief Class represents frame with methadata about content inside
*/
class Frame {
    void frameConv(void) {
        convert_endians(this->payload);
    }
public:
    uint32_t id;
    uint32_t payload;
    VideoData video;
    AudioList audio;
    uint32_t old_payload;
    /*!
     * @brief Method sets methadata, checks coding type and converts
     * methadata if it is big-endian
     * @param endian Information about coding type (little- or big- endian)
     * @param fin Input stream
     * @param fout Output stream
     * @param dst String - destination file name (for function close_files(...))
     * @return True if methadata (id of frame) are right
    */
    bool setFrame(uint32_t endian, fstream& fin, fstream& fout, const char*& dst) {
        if (this->id != FRAME_ID) {
            close_files(fin, fout, dst);
            return false;
        }
        if (endian == BIG_ENDIAN_ID) {
            frameConv();
        }
        this->old_payload = this->payload;
        return true;
    }
};

/*!
 * @brief Class represents global header with methadata about content inside
*/
class GlobalHead {
    void globConv(void) {
        convert_endians(this->len);
        convert_endians(this->payload);
    }
public:
    uint32_t id;
    uint32_t len;
    uint32_t payload;
    vector<Frame> frames;
    uint32_t old_payload;   //two sizes of payload are needed to right writing into file
                            //(if content will be changed)
    /*!
     * @brief Method sets methadata, checks coding type and converts
     * methadata if it is big-endian
     * @param endian Information about coding type (little- or big- endian)
     * @param fin Input stream
     * @param fout Output stream
     * @param dst String - destination file name (for function close_files(...))
     * @return True if methadata
     * (id of global head - must be LITTLE_ENDIAN or BIG_ENDIAN) are right
    */
    bool setGlobal(fstream& fin, fstream& fout, const char*& dst) {
        if (this->id == BIG_ENDIAN_ID) {
            globConv();
        }
        else if (this->id != LITTLE_ENDIAN_ID) {
            close_files(fin, fout, dst);
            return false;
        }
        this->old_payload = this->payload;
        this->frames.resize(this->len);
        return true;
    }
};

/*!
 * @brief Function to encode data if coding is big-endian
 * @param endian Information about endian-type
 * @param to_conv Data to convert
 * @param fin Input stream
 * @param fout Output stream
 * @param dst String - output file name
*/
void littleBigWrite(const uint32_t& endian, uint32_t& to_conv,
    fstream& fin, fstream& fout, const char*& dst) {
    if (endian == BIG_ENDIAN_ID) {
        convert_endians(to_conv);
    }
}
/*!
 * @brief Function to copy payloads from input to output file
 * @param fin Input stream
 * @param fout Output stream
 * @param payload Size of payload to copy
 * @param dst String - name of ouput file
 * @return True of writing was successful, else false
*/
bool writePayload(fstream & fin, fstream & fout, size_t payload, const char *& dst) {
    size_t max_alloc = 1000000;
    unsigned char* buffer = new unsigned char[max_alloc];
    while (payload >= max_alloc) {
        fin.read(reinterpret_cast<char*>(buffer), max_alloc);
        fout.write(reinterpret_cast<char*>(buffer), max_alloc);
        payload -= max_alloc;
    }
    if (payload > 0) {
        fin.read(reinterpret_cast<char*>(buffer), payload);
        fout.write(reinterpret_cast<char*>(buffer), payload);
    }
    delete[] buffer;
    if (!fout.good() || !fin.good()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}

bool write_to_file(fstream& fin, fstream& fout, GlobalHead& gl,
    const char*& lang, const char*& dst) {
    const size_t methadata_gl = 12;
    const size_t methadata_fr = 8;
    const size_t methadata_vi = 8;
    const size_t methadata_al = 12;
    const size_t methadata_ad = 10;
    const size_t crc_size = 4;
    uint32_t crc = 0;
    littleBigWrite(gl.id, gl.len, fin, fout, dst);
    littleBigWrite(gl.id, gl.payload, fin, fout, dst);
    fout.write(reinterpret_cast<char*>(&gl), methadata_gl);
    fin.seekg(methadata_gl, ios::cur);
    for (Frame f : gl.frames) {
        littleBigWrite(gl.id, f.payload, fin, fout, dst);
        fout.write(reinterpret_cast<char*>(&f), methadata_fr);
        fin.seekg(methadata_fr, ios::cur);
        littleBigWrite(gl.id, f.video.payload, fin, fout, dst);
        fout.write(reinterpret_cast<char*>(&f.video), methadata_vi);
        fin.seekg(methadata_vi, ios::cur);
        littleBigWrite(gl.id, f.video.payload, fin, fout, dst);
        writePayload(fin, fout, f.video.payload, dst);
        fout.write(reinterpret_cast<char*>(&crc), crc_size);
        fin.seekg(crc_size, ios::cur);
        littleBigWrite(gl.id, f.audio.len, fin, fout, dst);
        littleBigWrite(gl.id, f.audio.payload, fin, fout, dst);
        fout.write(reinterpret_cast<char*>(&f.audio), methadata_al);
        fin.seekg(methadata_al, ios::cur);
        littleBigWrite(gl.id, f.audio.payload, fin, fout, dst);
        for (AudioData a : f.audio.data_in) {
            if (a.lang[0] != lang[0] || a.lang[1] != lang[1]) {
                fin.seekg(methadata_ad, ios::cur);
                fin.seekg(a.payload, ios::cur);
                fin.seekg(crc_size, ios::cur);
            }
            else {
                char buffer[12];
                fin.read(reinterpret_cast<char*>(&buffer), methadata_ad);
                fout.write(reinterpret_cast<char*>(&buffer), methadata_ad);
                if (!writePayload(fin, fout, a.payload, dst)) {
                    return false;
                }
                fout.write(reinterpret_cast<char*>(&crc), crc_size);
                fin.seekg(crc_size, ios::cur);
            }
        }
        fout.write(reinterpret_cast<char*>(&crc), crc_size);
        fin.seekg(crc_size, ios::cur);
        fout.write(reinterpret_cast<char*>(&crc), crc_size);
        fin.seekg(crc_size, ios::cur);
    }
    fout.write(reinterpret_cast<char*>(&crc), crc_size);
    fin.seekg(crc_size, ios::cur);
    char buf;
    fin.get(buf);
    if (!fout.good() || !fin.eof()) {
        close_files(fin, fout, dst);
        return false;
    }
    return true;
}
/*!
 * @brief Function filters input fale and saves only wanted language
 * in every frame and it's audiodata inside
 * @param srcFileName String - source file name
 * @param dstFileName String - destination file name
 * @param lang String of two letters hat represents the search language
 * @return True if searching and wrinting information to destination
 * file was conducted successful, else false 
*/
bool filterFile(const char* srcFileName,
    const char* dstFileName,
    const char* lang) {
    fstream fin;
    fstream fout;
    GlobalHead gl;
    if (!open_files(fin, fout, srcFileName, dstFileName) ||
        !read_one(fin, fout, gl.id, dstFileName) ||
        !read_to(fin, fout, gl.len, gl.payload, dstFileName) ||
        !gl.setGlobal(fin, fout, dstFileName)) {
        return false;
    }
    size_t crc_size = 4;
    size_t need_fr_size = gl.old_payload;
    size_t methadata_fr = 8 + crc_size;
    for (size_t i = 0; i < gl.len; i++) {
        if (!read_to(fin, fout, gl.frames[i].id, gl.frames[i].payload, dstFileName) ||
            !gl.frames[i].setFrame(gl.id, fin, fout, dstFileName)) {
            return false;
        }
        need_fr_size -= (methadata_fr + gl.frames[i].old_payload);
        size_t methadata_v = 8 + crc_size;
        size_t methadata_al = 12 + crc_size;
        size_t need_val_size = gl.frames[i].old_payload;
        if (!read_to(fin, fout, gl.frames[i].video.id, gl.frames[i].video.payload, dstFileName) ||
            !gl.frames[i].video.setVideoData(gl.id, fin, fout, dstFileName)) {
            return false;
        }
        need_val_size -= (methadata_v + gl.frames[i].video.payload);
        fin.seekg(gl.frames[i].video.payload , ios::cur);
        fin.seekg(crc_size, ios::cur);
        if (!read_one(fin, fout, gl.frames[i].audio.id, dstFileName) ||
            !read_to(fin, fout, gl.frames[i].audio.len, gl.frames[i].audio.payload, dstFileName) ||
            !gl.frames[i].audio.setAudioList(gl.id, fin, fout, dstFileName)) {
            return false;
        }
        need_val_size -= (methadata_al + gl.frames[i].audio.old_payload);
        if (need_val_size != 0) {
            close_files(fin, fout, dstFileName);
            return false;
        }
        const uint32_t methadata_ad = 10 + crc_size;
        size_t need_ad_size = gl.frames[i].audio.old_payload;
        for (size_t j = 0; j < gl.frames[i].audio.old_len; j++) {
            if (!read_one(fin, fout, gl.frames[i].audio.data_in[j].id, dstFileName) ||
                !read_one(fin, fout, gl.frames[i].audio.data_in[j].lang, dstFileName) ||
                !read_one(fin, fout, gl.frames[i].audio.data_in[j].payload, dstFileName) ||
                !gl.frames[i].audio.data_in[j].setAudioData(gl.id, fin, fout, dstFileName)) {
                return false;
            }
            need_ad_size -= (methadata_ad + gl.frames[i].audio.data_in[j].payload);
            if (gl.frames[i].audio.data_in[j].lang[0] != lang[0] || 
                gl.frames[i].audio.data_in[j].lang[1] != lang[1]) {
                gl.frames[i].audio.payload -= (gl.frames[i].audio.data_in[j].payload + methadata_ad);
                gl.frames[i].audio.len--;
                gl.frames[i].payload -= (gl.frames[i].audio.data_in[j].payload + methadata_ad);
                gl.payload -= (gl.frames[i].audio.data_in[j].payload + methadata_ad);
            }
            fin.seekg(gl.frames[i].audio.data_in[j].payload, ios::cur);
            fin.seekg(crc_size, ios::cur);
        }
        if (need_ad_size != 0) {
            close_files(fin, fout, dstFileName);
            return false;
        }
        fin.seekg(crc_size, ios::cur);
        fin.seekg(crc_size, ios::cur);
    }
    fin.seekg(crc_size, ios::cur);
    if (!fin.good() || need_fr_size != 0 || fin.eof()) {
        close_files(fin, fout, dstFileName);
        return false;
    }
    char buf;
    fin.get(buf);
    if (!fin.eof()) {
        close_files(fin, fout, dstFileName);
        return false;
    }
    fin.clear();
    fin.seekg(0, ios::beg);
    fin.seekg(0, ios::end);
    size_t methadata_gl = 12 + crc_size;
    if ((int)fin.tellg() - methadata_gl - gl.old_payload != 0) {
        close_files(fin, fout, dstFileName);
        return false;
    }
    fin.clear();
    fin.seekg(0, ios::beg);
    if (!write_to_file(fin, fout, gl, lang, dstFileName)) {
        return false;
    }
    return true;
}

#ifndef __PROGTEST__
int main() {
    const char* in;
    const char* out;
   in = "sample\\big-endian\\in_0000.in";
    out = "outb0.out";
    filterFile(in, out, "cs");
    in = "sample\\in_0000.in";
    out = "outl0.out";
    filterFile(in, out, "cs");
    in = "sample\\big-endian\\in_0001.in";
    out = "outb1.out";
    filterFile(in, out, "cs");
    in = "sample\\in_0001.in";
    out = "outl1.out";
    filterFile(in, out, "cs");
    in = "sample\\big-endian\\in_0002.in";
    out = "outb2.out";
    filterFile(in, out, "en");
    in = "sample\\in_0002.in";
    out = "outl2.out";
    filterFile(in, out, "en");
    in = "sample\\in_0003.in";
    out = "outl3.out";
    filterFile(in, out, "en");
    in = "sample\\big-endian\\in_0003.in";
    out = "outb3.out";
    filterFile(in, out, "en");
    in = "sample\\in_0004.in";
    out = "outl4.out";
    filterFile(in, out, "pl");
    in = "sample\\big-endian\\in_0004.in";
    out = "outb4.out";
    filterFile(in, out, "pl");
    in = "sample\\in_0005.in";
    out = "outl5.out";
    filterFile(in, out, "cs");
    in = "sample\\big-endian\\in_0005.in";
    out = "outb5.out";
    filterFile(in, out, "cs");
    in = "sample\\in_my.in";
    out = "out_my.out";
    filterFile(in, out, "en");
    in = "sample\\in_my0.in";
    out = "out_my0.out";
    filterFile(in, out, "en");
    in = "in_4003073.bin";
    out = "bin.out";
    filterFile(in, out, "en");
    in = "in_4003249.bin";
    out = "bin0.out";
    filterFile(in, out, "cs");
    return 0;
}
#endif /* __PROGTEST__ */

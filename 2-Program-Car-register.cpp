#ifndef __PROGTEST__
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <string>
#include <memory>
#include <functional>
#include <vector>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */
/*!
 * @brief CPerson class represents person with his name and surname,
 * and list of his cars
*/

class CPerson {
public:
    CPerson(const string& nm, const string& srnm) {
        this->m_Name = nm;
        this->m_Surname = srnm;
    }
    CPerson(const string& nm, const string& srnm, const string& rz) {
        this->m_Name = nm;
        this->m_Surname = srnm;
        this->addCar(rz);
    }
    /*!
     * @brief Method helps get to know what name has person
     * @return Name of person (string)
    */
    const string& getName() const {
        return this->m_Name;
    }
    /*!
    * @brief Method makes clear what surname has person
    * @return Surname of person (string)
    */
    const string& getSurname() const {
        return this->m_Surname;
    }
    /*!
     * @brief Method suggests what cars has person
     * @return List of person's cars (vector)
    */
    const vector<string>& getCars() const {
        return this->m_Cars;
    }

    /*!
    * @brief Method suggests how much cars has person
    * @return Number of cars
    */
    int getNumCars() const {
        return this->m_Cars.size();
    }
    /*!
     * @brief Method tells if there is car with given
     * registration plate
     * @param rz Registration plate (string)
     * @return True if one of reg. plates is same as the search plate,
     * else false
    */
    bool hasCarRZ(const string& rz) const {
        return find(this->m_Cars.begin(), this->m_Cars.end(), rz)
            != this->m_Cars.end();
    }
    /*!
     * @brief Method adds car with given registration
     * plate to list of person's cars
     * @param rz Registration plate (string)
    */
    void addCar(const string& rz) {
        this->m_Cars.push_back(rz);
    }
    /*!
     * @brief Method deletes car with given registration
     * plate from list of some person's cars
     * @param rz Registration plate (string)
    */
    void delCar(const string& rz) {
        this->m_Cars.erase(remove(this->m_Cars.begin(),
            this->m_Cars.end(), rz), this->m_Cars.end());
    }
private:
    string m_Name;
    string m_Surname;
    vector<string> m_Cars;
};
/*!
 * @brief CCarList class is created for one-time
 * passing of list of person's cars
*/
class CCarList {
public:
    /*!
     * @brief Every of two constructors helps to create right
     * structure and iterator for list of cars
     *
     * There are not parameters required in first case.
     *
     * In second case:
     * @param cars List of cars (vector)
    */
    CCarList() {
        this->it = this->m_Cars.begin();
    }
    CCarList(const vector<string>& cars) {
        this->m_Cars = cars;
        this->it = this->m_Cars.begin();
    }
    const string& RZ(void) const;
    bool           AtEnd(void) const;
    void           Next(void);
private:
    vector<string> m_Cars;
    vector<string>::iterator it;
};


/*!
 * @brief Method makes clear what registration plate
 * has car that the iterator is pointing to
 * @return Registration plate (string)
*/
const string& CCarList::RZ(void) const {
    return *(this->it);
}

/*!
 * @brief Method makes clear if iterator is pointing
 * to the end of cars' list
 * @return True if the end was reached, false else
*/
bool CCarList::AtEnd(void) const {
    return this->it == this->m_Cars.end();
}

/*!
 * @brief Method moves iterator to next position
*/
void CCarList::Next(void) {
    this->it++;
}


/*!
 * @brief CPersonList class represents list
 * of people in our database of cars and iterator to pass the list by
*/
class CPersonList {
public:
    /*!
     * @brief Constructors help to create right
     * structure and iterator for list of people
     *
     * There are not parameters required in first case.
     *
     * In second case:
     * @param cars List of people(vector)
    */
    CPersonList() {
        vector<CPerson> p;
        this->m_People = p;
        this->it = this->m_People.begin();
    }
    CPersonList(const vector<CPerson>& people) {
        this->m_People = people;
        this->it = this->m_People.begin();
    }
    const string& Name(void) const;
    const string& Surname(void) const;
    bool           AtEnd(void) const;
    void           Next(void);
private:
    vector<CPerson> m_People;
    vector<CPerson>::iterator it;
};

/*!
 * @brief Method makes clear what name
 * has person that the iterator is pointing to
 * @return Name of person (string)
*/
const string& CPersonList::Name(void) const {
    return (*(this->it)).getName();
}

/*!
 * @brief Method makes clear what surname
 * has person that the iterator is pointing to
 * @return Surname of person (string)
*/

const string& CPersonList::Surname(void) const {
    return (*(this->it)).getSurname();
}

/*!
 * @brief Method makes it clear if iterator is pointing
 * to the end of people's list
 * @return True if the end was reached, else false
*/
bool CPersonList::AtEnd(void) const {
    return this->it == this->m_People.end();
}

/*!
 * @brief Method moves iterator to next position
*/
void CPersonList::Next(void) {
    this->it++;
}

/*!
 * @brief CRegister class represents all informaition of cars
 * and their owners recorded in database
*/
class CRegister {
public:
    CRegister(void);
    ~CRegister(void);
    CRegister(const CRegister& src) = delete;
    CRegister& operator = (const CRegister& src) = delete;
    bool        AddCar(const string& rz,
        const string& name,
        const string& surname);
    bool        DelCar(const string& rz);
    bool        Transfer(const string& rz,
        const string& nName,
        const string& nSurname);
    CCarList    ListCars(const string& name,
        const string& surname) const;
    int         CountCars(const string& name,
        const string& surname) const;
    CPersonList ListPersons(void) const;
private:
    vector<CPerson> m_Owners;
};

/*!
 * @brief Function helps to compare names and surname of
 * two people (it is useful for sortion of people list)
 * @param p1 First person (CPerson)
 * @param p2 Second person (CPerson)
 * @return True if pair "name-surname" of person one must
 * be before if same pair of person two in alphabetical order
*/
bool cmpLetters(const CPerson& p1, const CPerson& p2) {
    const string& srnm1 = p1.getSurname();
    const string& srnm2 = p2.getSurname();
    return srnm1 < srnm2 ||
        (srnm1 == srnm2 && p1.getName() < p2.getName());
}

/*!
 * @brief Constructor for CRegister object
*/
CRegister::CRegister(void) {}

/*!
 * @brief Destructor for CRegister object
*/
CRegister::~CRegister(void) {}

/*!
 * @brief Methods adds car with this registration plate to cars of
 * person with this name and surname
 * @param rz Registration plate (string)
 * @param name Name of person (string)
 * @param surname Surname of person (string)
 * @return True if adding was successful, false if mistake was noticed
 *
 * Possible mistakes: reg. plate already exists in database
*/
bool CRegister::AddCar(const string& rz,
    const string& name,
    const string& surname) {
    vector<CPerson>::iterator sta = this->m_Owners.begin();
    vector<CPerson>::iterator fin = this->m_Owners.end();
    for (vector<CPerson>::iterator it = sta; it < fin; it++) {
        if ((*it).hasCarRZ(rz)) {
            return false;
        }
    }
    CPerson person(name, surname, rz);
    vector<CPerson>::iterator it = lower_bound(sta, fin, person, cmpLetters);
    if (it == this->m_Owners.end() ||
        (*it).getName() != name || (*it).getSurname() != surname) {
        this->m_Owners.insert(it, person);
        return true;
    }
    (*it).addCar(rz);
    return true;
}
/*!
 * @brief Method deletes car with given registration plate from database
 * @param rz Registration plate (string)
 * @return True if operation was conducted successfully, false if mistake has been detected
 *
 * Possible mistakes: car with this reg. plate is not in database
*/
bool CRegister::DelCar(const string& rz) {
    vector<CPerson>::iterator sta = this->m_Owners.begin();
    vector<CPerson>::iterator fin = this->m_Owners.end();
    for (vector<CPerson>::iterator it = sta; it < fin; it++) {
        if ((*it).hasCarRZ(rz)) {
            (*it).delCar(rz);
            if ((*it).getNumCars() == 0) {
                this->m_Owners.erase(it);
            }
            return true;
        }
    }
    return false;
}
/*!
 * @brief Method helps to transfer car with reg. plate from current owner to new
 * @param rz Registration plate (string)
 * @param nName Name of new owner (string)
 * @param nSurname Surname of new owner (string)
 * @return True if car was tranfered successfully, else if one of mistakes
 * have been recorded false
 *
 * Possible mistakes: car with this reg. plate doesn't exists in database,
 * current and new owners are the same person
*/
bool CRegister::Transfer(const string& rz,
    const string& nName, const string& nSurname) {
    CPerson person(nName, nSurname, rz);
    vector<CPerson>::iterator it = lower_bound(this->m_Owners.begin(),
        this->m_Owners.end(), person, cmpLetters);
    if (it != this->m_Owners.end()) {
        if ((*it).getSurname() == nSurname && (*it).getName() == nName) {
            if ((*it).hasCarRZ(rz) || !this->DelCar(rz)) {
                return false;
            }
            it = lower_bound(this->m_Owners.begin(),
                this->m_Owners.end(), person, cmpLetters); //updating poiter after deletion
            (*it).addCar(rz);
            return true;
        }
        if (!this->DelCar(rz)) {
            return false;
        }
        if (this->m_Owners.size() == 0) {
            this->m_Owners.push_back(person);
            return true;
        }
        it = lower_bound(this->m_Owners.begin(),
                this->m_Owners.end(), person, cmpLetters); //updating poiter after deletion
        this->m_Owners.insert(it, person);
        return true;
    }
    if (!this->DelCar(rz)) {
        return false;
    }
    this->m_Owners.push_back(person);
    return true;
}

/*!
 * @brief Method creates list of person's cars for one-time passing
 * @param name Name of person (string)
 * @param surname Surname of person (string)
 * @return CCarList object - list of cars and iterator to pass by
*/
CCarList CRegister::ListCars(const string& name,
    const string& surname) const {
    CPerson person(name, surname);
    vector<CPerson>::const_iterator it = lower_bound(this->m_Owners.begin(), this->m_Owners.end(), person, cmpLetters);
    if (it != this->m_Owners.end() && ((*it).getName() == name && (*it).getSurname() == surname)) {
        CCarList cars((*it).getCars());
        return cars;
    }
    CCarList car;
    return car;
}

/*!
 * @brief Method helps to know how mush cars has person
 * @param name Name of person (string)
 * @param surname Surname of person (string)
 * @return Number of person's cars (0 if person is not in database)
*/
int CRegister::CountCars(const string& name,
    const string& surname) const {
    CPerson person(name, surname);
    vector<CPerson>::const_iterator it = lower_bound(this->m_Owners.begin(), this->m_Owners.end(), person, cmpLetters);
    if (it != this->m_Owners.end() && ((*it).getName() == name && (*it).getSurname() == surname)) {
        return (*it).getNumCars();
    }
    return 0;
}

/*!
 * @brief Method creates list of people for one-time passing
 * @return CPersonList object - list of people and iterator to pass by
*/
CPersonList CRegister::ListPersons(void) const {
    CPersonList people(this->m_Owners);
    return people;
}


#ifndef __PROGTEST__

int main(void)
{
    return 0;
}
#endif /* __PROGTEST__ */

//WARNING: Programm does ot include comments!
//Using STL not allowed by teachers of this course, so there are wroten some structures and classes by myself

#include <iostream>
#include <cmath>
using namespace std;

void getPotencialSet(bool ** poset, bool ** rules, const int & n, const int & v){
    for (int i = 0; i < n; ++i) {
        poset[0][i] = false;
    }
    bool * implXOR = new bool[n];
    int deg = 2;
    int r_deg = 0;
    if( v > 1 ) {
        for (int i = 0; i < n; ++i) {
            implXOR[i] = rules[0][i];
        }
    }
    for (int i = 1; i < v; ++i) {
        if( i == deg ){
            r_deg++;
            deg *= 2;
            for ( int j = 0; j < n; ++j ) {
                implXOR[j] = rules[r_deg][j];
            }
        }
        for (int j = 0; j < n; ++j) {
            poset[i][j] = implXOR[j] ^ poset[i - deg/2][j];
        }
    }
    delete [] implXOR;
}

void fillRule(bool * rule, int n){
    char tmp;
    for (int j = 0; j < n; ++j)
        rule[j] = (cin >> tmp && tmp == '1');
}

struct Node{
    int x;
    int y;
    Node *Next;
};

class Queue{
    Node *head, *tail;
public:
    Queue();
    ~Queue();
    void Add(int x, int y);
    void Del();
    bool isEmpty();
    int getX(){
        if(head != nullptr) return this->head->x;
        return -1;
    }
    int getY(){
        if(head != nullptr) return this->head->y;
        return -1;
    }
};

Queue::Queue() {
    this->head = nullptr;
    this->tail = head;
}

Queue::~Queue() {
    Node *temp = head;
    while(temp != nullptr){
        temp = head->Next;
        delete head;
        head = temp;
    }
}

void Queue::Add(int x, int y) {
    Node *temp = new Node;
    temp->x = x;
    temp->y = y;
    temp->Next = nullptr;

    if(head != nullptr){
        tail->Next = temp;
        tail = temp;
    }
    else head = tail = temp;
}

void Queue::Del() {
    if (head != nullptr){
        Node *temp = head;
        head = head->Next;
        delete temp;
    }
}

bool Queue::isEmpty(){
    return head == nullptr;
}

class GraphNode{
public:
    int len;
    bool visited;
};

bool bfs(GraphNode * graph, int n, int fin_y, int fin_x){
    Queue queue;
    queue.Add(0, 0);
    if(graph[fin_x * n + fin_y].visited ) return false;
    graph[0].visited = true;
    if(fin_x == 0 && fin_y == 0) return true;
    while(!queue.isEmpty()){
        int x = queue.getX();
        int y = queue.getY();
        if(x - 1 >= 0 && !graph[n * (x - 1) + y].visited) {
            graph[n * (x - 1) + y].len += graph[x * n + y].len + 1;
            queue.Add(x - 1, y);
            graph[n * (x - 1) + y].visited = true;
            if(x - 1 == fin_x && y == fin_y){
                return true;
            }
        }
        if(y % n != 0 && (y - 1) >= 0 && !graph[n * x + (y - 1)].visited) {
            graph[n * x + (y - 1)].len += graph[x * n + y].len + 1;
            queue.Add(x, (y - 1));
            graph[n * x + (y - 1)].visited = true;
            if(x == fin_x && y - 1 == fin_y){
                return true;
            }
        }
        if(x + 1 < n && !graph[n * (x + 1) + y].visited) {
            graph[n * (x + 1) + y].len += graph[x * n + y].len + 1;
            queue.Add((x + 1), y);
            graph[n * (x + 1) + y].visited = true;
            if(x + 1 == fin_x && y == fin_y){
                return true;
            }
        }
        if((y + 1) % n != 0 && y + 1 < n && !graph[n * x + y + 1].visited) {
            graph[n * x + y + 1].len += graph[x * n + y].len + 1;
            queue.Add(x, y + 1);
            graph[n * x + y + 1].visited = true;
            if(x == fin_x && y + 1 == fin_y){
                return true;
            }
        }
        queue.Del();
    }
    return false;
}

void constructPath(GraphNode * graph, int n, int fin_y, int fin_x) {
    int counter = graph[fin_x * n + fin_y].len;
    int final = counter;
    int * out = new int[counter];
    counter--;
    out[counter] = fin_x * n + fin_y;
    while(counter > 0){
        if ((fin_x * n + fin_y) % n != 0 && fin_x * n + fin_y - 1 >= 0 && graph[fin_x * n + fin_y - 1].len == counter){
            fin_y--;
        }
        else if (fin_x * n + fin_y + 1 < n * n && (fin_x * n + fin_y + 1) % n != 0 && graph[fin_x * n + fin_y + 1].len == counter){
            fin_y++;
        }
        else if ((fin_x - 1) * n + fin_y >= 0 &&graph[(fin_x - 1) * n + fin_y].len == counter){
            fin_x--;
        }
        else if ((fin_x + 1) * n + fin_y < n * n && graph[(fin_x + 1) * n + fin_y].len == counter){
            fin_x++;
        }
        counter--;
        out[counter] = fin_x * n + fin_y;
    }
//    for (int i = 0; i < n; i++) {
//        for(int j = 0; j < n; j++){
//             cout << graph[i*n+j].visited;
//            printf("%d[%d;%d] ", graph[i*n+j].len, i, j);
//        }
//        cout << '\n';
//    }
    cout << "[1;1]";
    for (int i = 0; i < final; i++) {
        printf(",[%d;%d]", out[i]%n + 1, out[i]/n + 1);
    }
    delete [] out;
}

void cpGraph(GraphNode * g, GraphNode * gc, int n){
    for (int i = 0; i < n * n; i++) {
        gc[i] = g[i];
    }
}

void modifyGraph (GraphNode * graph, const bool * permutation, int n){
    for(int i = 0; i < n; i++){
        if(permutation[i]){
            for (int j = i; j < n * n; j += n)
                graph[j].visited = !(graph[j].visited);
        }
    }
}

int main() {
    int n, k;
    cin >> n >> k;
    int v = (int) pow(2, k);
    bool ** poset = new bool * [v];
    for(int i = 0; i < v; i++){
        poset[i] = new bool [n];
    }
    bool ** rules;
    int positions[10];
    if ( k > 0 ) {
        rules = new bool *[k];
        for (int i = 0; i < k; ++i) {
            rules[i] = new bool[n];
        }
    }
    for (int i = 0; i < k; i++) {
        cin >> positions[i];
        fillRule(rules[i], n);
    }
    getPotencialSet(poset, rules, n, v);
    char tmp;
    auto *graph = new GraphNode[n * n];
    //fill field with barriers
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> tmp;
            if (tmp == '1') {
                graph[i * n + j].visited = true;
                graph[i * n + j].len = 0;
            } else {
                graph[i * n + j].visited = false;
                graph[i * n + j].len = 0;
            }
        }
    }
    int fin_x, fin_y;
    cin >> fin_x >> fin_y;
    fin_x--;
    fin_y--;
    auto *graphcp = new GraphNode[n * n];
    cpGraph(graph, graphcp, n);
    auto *graphorig = new GraphNode[n * n];
    cpGraph(graph, graphorig, n);
    int fin_len = n * n;
    int fin_toggles = 0;
    int i = 0;
    while (i < v) {
        modifyGraph(graph, poset[i], n);
        if (graph[0].visited || !bfs(graph, n, fin_x, fin_y)) {
            cpGraph(graphorig, graph, n);
            i++;
            continue;
        }
        int hlp = 0, add = 0, d = 0;
        for ( int helping_i = i; helping_i > 0; helping_i /= 2){
            if (helping_i % 2 == 1 && positions[d] != 0){
                if(positions[d] == -1){
                    add = 2;
                }
                else if( hlp < positions[d] * 2 ) hlp = positions[d] * 2;
            }
            d ++;
        }
        hlp += add;
        if (graph[fin_y * n + fin_x].len + hlp < fin_len  || fin_len == n * n) {
            cpGraph(graph, graphcp, n);
            fin_len = graph[fin_y * n + fin_x].len + hlp;
            fin_toggles = i;
        }
        cpGraph(graphorig, graph, n);
        i++;
    }
    if ( fin_len == n * n ) {
        if(k>0){
            for (int j = 0; j < k; ++j) {
                delete [] rules[j];
            }
            delete [] rules;
        }
        for(int o = 0; o < v; o++){
            delete [] poset[o];
        }
        delete [] poset;
        delete[] graph;
        delete[] graphcp;
        delete[] graphorig;
        cout << -1;
        return -1;
    }
    cout << fin_len << '\n';
    for (int j = 0; j < k; j++) {
        cout << fin_toggles % 2;
        fin_toggles /= 2;
    }
    if (k > 0) cout << '\n';
    constructPath(graphcp, n, fin_x, fin_y);
    if(k>0){
        for (int j = 0; j < k; ++j) {
            delete [] rules[j];
        }
        delete [] rules;
    }
    delete[] graph;
    delete[] graphcp;
    delete[] graphorig;
    for(int o = 0; o < v; o++){
        delete [] poset[o];
    }
    delete [] poset;
    return 0;
}